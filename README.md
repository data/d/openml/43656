# OpenML dataset: 2018-CDCs-Social-Vulnerability-Index-US-Counties

https://www.openml.org/d/43656

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Full Data Dictionary and Documentation 
Social vulnerability refers to the resilience of communities when confronted by external stresses on human health, stresses such as natural or human-caused disasters, or disease outbreaks. Reducing social vulnerability can decrease both human suffering and economic loss. CDC's Social Vulnerability Index uses 15 U.S. census variables at tract level to help local officials identify communities that may need support in preparing for hazards; or recovering from disaster.
The Geospatial Research, Analysis, and Services Program (GRASP) created and maintains CDCs Social Vulnerability Index.
Content
This dataset has more than 30  features across 4 major focus areas.

Inspiration
This data has a large feature set that can be useful for a myriad of analytics. It may provide particular use to the current COVID-19 analysis.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43656) of an [OpenML dataset](https://www.openml.org/d/43656). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43656/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43656/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43656/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

